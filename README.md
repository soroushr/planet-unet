Application of deep learning for iceberg detection in Greenland fjords using [Planet imagery](https://www.planet.com/).

[Tensorflow implementation of UNet](https://github.com/jakeret/tf_unet) developed by Joel Akeret is [modified and used](https://bitbucket.org/soroushr/planet-unet/src/master/tf_unet/) in this repo.
